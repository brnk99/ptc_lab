library ieee;
use ieee.std_logic_1164.all;

entity adder_nbit is 
generic(n: natural);
port(
	a: in std_logic_vector(n-1 downto 0);
	b: in std_logic_vector(n-1 downto 0);
	carry_in: in std_logic;
	s: out std_logic_vector(n-1 downto 0);
	carry: out std_logic
);
end adder_nbit;

architecture struct of adder_nbit is
	component sumator
		port(a: in std_logic; b: in std_logic; cin: std_logic; s: out std_logic; cout: out std_logic);
	end component;
	signal carry_array: std_logic_vector(0 to n-1);
	begin
		sum0: sumator port map (a(0), b(0), carry_in, s(0), carry_array(0));

		sum_gen: for i in 1 to n-2 generate
			begin
				sum: sumator port map (a(i), b(i), carry_array(i-1), s(i), carry_array(i));
			end generate;
			
		sumx: sumator port map (a(n-1), b(n-1), carry_array(n-2), s(n-1), carry);

end struct;
	

