library ieee;
use ieee.std_logic_1164.all;

entity register_nbit is 
generic(n: natural := 8);
port(
	clk: in std_logic;
	rst: in std_logic;
	ain: in std_logic_vector(n-1 downto 0);
	aout: out std_logic_vector(n-1 downto 0)
);
end register_nbit;

architecture struct of register_nbit is
	component rejestr1bit
		port(clk: in std_logic; rst: in std_logic; ain: in std_logic; aout: out std_logic);
	end component;
	begin
		rejestr_gen: for i in 0 to n-1 generate
		begin
			rejestr: rejestr1bit port map (clk, rst, ain(i), aout(i));
	end generate;
end struct;


	

