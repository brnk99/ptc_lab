library ieee;
use ieee.std_logic_1164.all;


entity zad5_4 is 
	generic(n: natural := 8);
	port(
	key: in std_logic_vector(2 downto 0);
	SW: in std_logic_vector(17 downto 0);
	hex2, hex3, hex4, hex5, hex6, hex7: out std_logic_vector(6 downto 0);
	LEDR: out std_logic_vector(n downto 0)
	);
end zad5_4;


architecture struct of zad5_4 is

	component adder_seq
		generic(n: natural := n);
		port(
			a_in, b_in: in std_logic_vector(n-1 downto 0);
			clk, rst, sel, addsub: in std_logic;
			result: out std_logic_vector(n-1 downto 0);
			overflow: out std_logic
		);
	end component;
	
	component display_hex is 
		port(
			a: in std_logic_vector(3 downto 0):="0000";
			display: out std_logic_vector(6 downto 0)
		);
	end component;
	
	signal a: std_logic_vector(n-1 downto 0);
	signal b: std_logic_vector(n-1 downto 0);

	signal clk: std_logic;
	signal rst: std_logic;
	signal sel: std_logic;
	signal addsub: std_logic;
	signal carry: std_logic;
	signal result: std_logic_vector(n-1 downto 0);
	begin
		clk <= key(1);
		rst <= not key(2);
		sel <= sw(16);
		addsub <= sw(17);
		
		a <= SW((n*2)-1 downto n);
		b <= SW(n-1 downto 0);
		
		adder: adder_seq port map (
			a, b,
			clk, rst, sel, addsub,
			result,
			carry
		);
		
		ledr(0) <= carry;
		
		d0: display_hex port map (result(3 downto 0), hex2);
		d1: display_hex port map (result(7 downto 4), hex3);
		
		d2: display_hex port map (b(3 downto 0), hex4);
		d3: display_hex port map (b(7 downto 4), hex5);
		
		d4: display_hex port map (a(3 downto 0), hex6);
		d5: display_hex port map (a(7 downto 4), hex7);
		
end struct;