library ieee;
use ieee.std_logic_1164.all;

entity zad5_sumator_sekwencyjny is 
	port(
	KEY0: in std_logic;
	SW: in std_logic_vector(15 downto 0);
	LEDR: out std_logic_vector(9 downto 0)
	);
end zad5_sumator_sekwencyjny;

architecture struct of zad5_sumator_sekwencyjny is
	component sumator8bit
		port(
		a: in std_logic_vector(7 downto 0);
		b: in std_logic_vector(7 downto 0);
		cin: in std_logic;
		s: out std_logic_vector(7 downto 0);
		cout: out std_logic
	);
	end component;
	
	component rejestr8bit
		port(
		clk: in std_logic;
		ain: in std_logic_vector;
		aout: out std_logic_vector
	);
	end component;
	
	component rejestr1bit
		port(clk: in std_logic; ain: in std_logic; aout: out std_logic);
	end component;
	
	TYPE array_vector IS ARRAY (2 DOWNTO 0) OF std_logic_vector(7 downto 0);

	signal vectors: array_vector; 
	signal A: std_logic_vector(7 downto 0) := SW(15 downto 8);
	signal B: std_logic_vector(7 downto 0) := SW(7 downto 0);
	signal CLK: std_logic := KEY0;
	signal cout: std_logic;

	begin
		rejestr0: rejestr8bit port map (CLK, A, vectors(0));
		rejestr1: rejestr8bit port map (CLK, B, vectors(1));
		rejestr3: rejestr8bit port map (CLK, vectors(2), LEDR(7 downto 0));
		
		dff: rejestr1bit port map (CLK, cout, LEDR(9));

		sum0: sumator8bit port map (
			vectors(0),
			vectors(1),
			'0',
			vectors(2),
			cout
		);
		
		
end struct;